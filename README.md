## Synopsis

Marsonsoft.Eye takes care of organizing your digital photo and home video files into a simple year/month directory structure. Nothing else. It does **not** index your files, it does **not** maintain any media data, and it does **not** help you search for all photos of uncle Jim.



## How it works
I created this program because all the files for my digital home photos and videos were usually crowded together in various folders like this:
```
Summer 2017\
The Festival\
Selling stuff\
Jims birthday\
Jims birthday - copy\
```
...but a lot more of them and endless number of copies.

I thought if I could simply sort the files into some meaningful structure my life would become easier and my precious files would become more manageable. The structure could be something like this:
```
2017\
  01\
  02\
  ...
  12\
2018\
  01\
  02\
  ...
```
This is exactly what Marsonsoft.Eye does for you. It uses date and time information from the media files to automatically sort the files into the year/month structure. In order to do this, you have to tell Marsonsoft.Eye two things:

1. The directory where your files should be stored, called the `RepositoryDirectoryPath`
2. The directory where you will put all new photo and video files that should be organized, called the `ScanDirectoryPath`.

The `ScanDirectoryPath` will contain two folders:

```
ScanDirectoryPath\
  Inbox\
  Outbox\
```

Once the Marsonsoft.Eye service (see below) is up and running, it watches the Inbox directory for all incoming media files. Each file is moved from the Inbox to the correct location in  the `RepositoryDirectoryPath` directory. If something fails or Marsonsoft.Eye detects that the file has already been put into the repository, the file is moved to the Outbox. 



## Building ##

1.  Clone the git repository into a local folder, for example, c:\gitlab\Eye
2.  Build the solution using
    
    ```bat
    cd c:\gitlab\Eye
    msbuild /p:Configuration=Release
    ```


## Installing

1.  Build the project or download the .zip file containing the binaries.
2.  Select a directory to store the running service, for example, c:\tools\EyeService
3.  If you downloaded the zip file, extract it Extract the .zip file to c:\tools\EyeService 
4.  If you built the solution, copy the binaries of the service to your directory:
    
    ```shell
    xcopy C:\gitlab\Eye\src\netframework\EyeService\bin\Release c:\tools\EyeService\ /y /i
    ```

5.  Edit the C:\tools\EyeService\EyeService.exe.config file and
    - set `RepositoryDirectoryPath` to the directory where your files should be stored
    - set `ScanDirectoryPath` to the directory that will hold your Inbox and Outbox directories for incoming files.
6.  Install the program as a Windows service:
    
    ```bat
    installutil C:\tools\EyeService\EyeService.exe
	OR
	C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe C:\tools\EyeService\EyeService.exe
    ```

7.  Ensure the service is started by running the following in an Administrator console:
    
    ```bat
    sc start EyeService
    ```



## License

The project is licensed under the MIT license.

