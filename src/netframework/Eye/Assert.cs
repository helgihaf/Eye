﻿using Marsonsoft.Eye.Abstractions;
using System;

namespace Eye
{
    internal static class Assert
    {
        public static bool Equal(string s1, string s2, string contextMsg)
        {
            bool result = object.Equals(s1, s2);
            if (!result)
            {
                LogFailure(contextMsg, s1, s2, "");
            }

            return result;
        }

        public static bool Equal(byte[] b1, byte[] b2, string contextMsg)
        {
            if (b1 == null && b2 == null)
            {
                return true;
            }

            bool result = false;

            if (b1 != null && b2 != null && b1.Length == b2.Length)
            {
                result = true;
                for (int i = 0; i < b1.Length && result; i++)
                {
                    result = b1[i] == b2[i];
                }
            }

            if (!result)
            {
                LogFailure(contextMsg, "Byte arrays differ.");
            }

            return result;
        }

        public static void Equal(DateTime dateTime1, DateTime dateTime2, double allowedDifferenceMilliseconds, string contextMsg)
        {
            var ts = dateTime1 - dateTime2;
            var difference = Math.Abs(ts.TotalMilliseconds);
            if (difference > allowedDifferenceMilliseconds)
            {
                LogFailure(contextMsg, $"Found a difference of {ts.Days} days, {ts.Hours} and {ts.Minutes}");
            }
        }

        public static bool Equal<T>(T value1, T value2, string contextMsg)
        {
            bool result = object.Equals(value1, value2);
            if (!result)
            {
                LogFailure(contextMsg, value1.ToString(), value2.ToString(), "");
            }

            return result;
        }

        public static void LogFailure(string contextMsg, string v1, string v2, string msg)
        {
            LogFailure(contextMsg, $"Expected {v1} but got {v2}. {msg}");
        }

        public static void LogFailure(string contextMsg, string msg)
        {
            Log.Error($"{contextMsg}: {msg}");
        }

    }
}
