﻿using Marsonsoft.CommandLineParser;
using Marsonsoft.Eye.Abstractions;
using Marsonsoft.Eye.Core;
using Marsonsoft.Eye.Framework;
using Marsonsoft.Eye.MediaInfoAdapters;
using System;
using System.Linq;

namespace Eye
{
    class Program
    {
        // eye verify <path>
        // eye scan [-move] <target> <source>
        static void Main(string[] args)
        {
            if (args.Length <= 1)
            {
                Console.WriteLine("Arguments missing");
                return;
            }

            var parserSetup = ParserSetup.CreatePowershellStyle();
            Log.Loggers.Add(new ConsoleLogger());

            var adapterFactory = new MediaInfoAdapterFactory();
            adapterFactory.Install("image", new ImageAdapter());
            adapterFactory.Install("video", new VideoAdapter());

            var fileAnalyzer = new FileAnalyzer(adapterFactory);

            if (args[0] == "verify")
            {
                var verifyCommand = new VerifyCommand();
                verifyCommand.Run(args.Skip(1), parserSetup, fileAnalyzer);
            }
            else if (args[0] == "scan")
            {
                var scanCommand = new ScanCommand();
                scanCommand.Run(args.Skip(1), parserSetup, fileAnalyzer);
            }
        }
    }
}
