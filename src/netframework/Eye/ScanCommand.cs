﻿using Marsonsoft.CommandLineParser;
using Marsonsoft.Eye.Abstractions;
using Marsonsoft.Eye.Core;
using Marsonsoft.Eye.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Eye
{
    internal class ScanCommand
    {
        public void Run(IEnumerable<string> args, ParserSetup parserSetup, FileAnalyzer fileAnalyzer)
        {
            var parser = new Parser(parserSetup);
            var scanParameters = parser.Parse<ScanCommandParameters>(args.ToArray());

            Scan(scanParameters, fileAnalyzer);
        }

        private void Scan(ScanCommandParameters scanParameters, FileAnalyzer fileAnalyzer)
        {
            Console.WriteLine("Building repository...");

            var repository = BuildRepository(scanParameters.TargetPath, fileAnalyzer);

            var fileEnumerator = new FileEnumerator(scanParameters.SourcePath, scanParameters.Recurse, 30);

            foreach (var filePath in fileEnumerator.EnumerateFiles())
            {
                var fileData = fileAnalyzer.Analyze(filePath);
                if (fileData != null)
                {
                    try
                    {
                        repository.AddFile(filePath);
                    }
                    catch (ArgumentException ex)
                    {
                        Log.Warning(ex.Message);
                    }
                }
                else
                {
                    Log.Warning($"Cannot analyze file {filePath}");
                }
            }
        }

        private FileRepository BuildRepository(string targetPath, FileAnalyzer fileAnalyzer)
        {
            Directory.CreateDirectory(targetPath);
            var fileEnumerator = new FileEnumerator(targetPath, true, 10);
            var fileOperator = new FileOperator();
            var repository = new FileRepository(fileAnalyzer, fileEnumerator, fileOperator);
            repository.Build();

            return repository;
        }
    }
}
