﻿using Marsonsoft.CommandLineParser;

namespace Eye
{
    public class ScanCommandParameters
    {
        [Parameter(Name = "recurse", Aliases = new[] { "r" }, Description = "Recurse subdirectories")]
        public bool Recurse { get; set; }

        [Parameter(Name = "progress", Aliases = new[] { "p" }, Description = "Log progress")]
        public bool LogProgress { get; set; }

        [Parameter(Name = "move", Aliases = new[] { "m" }, Description = "Move files from source into target instead of just copying them")]
        public bool Move { get; set; }

        [Parameter(Description = "Target path", Position = 0)]
        public string TargetPath { get; set; }

        [Parameter(Description = "Source path", Position = 1)]
        public string SourcePath { get; set; }
    }
}
