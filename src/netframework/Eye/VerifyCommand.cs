﻿using Marsonsoft.CommandLineParser;
using Marsonsoft.Eye.Abstractions;
using Marsonsoft.Eye.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Eye
{
    internal class VerifyCommand
    {
        public void Run(IEnumerable<string> args, ParserSetup parserSetup, FileAnalyzer fileAnalyzer)
        {
            var parser = new Parser(parserSetup);
            var verifyParameters = parser.Parse<VerifyCommandParameters>(args.ToArray());

            var fileEnumerator = new FileEnumerator(verifyParameters.Path, verifyParameters.Recurse, 10);
            foreach (var path in fileEnumerator.EnumerateFiles())
            {
                var fileData = fileAnalyzer.Analyze(path);
                if (fileData != null)
                {
                    var verifyFileData = new VerifyFileData { Path = path };
                    AssertEqual(verifyFileData, fileData);
                }
            }

            //Console.WriteLine("Collecting files...");
            //var filePaths = await CollectFiles(verifyParameters.Path, verifyParameters.Recurse);

            //int total = filePaths.Count;
            //int current = 0;
            //var lastTime = DateTime.UtcNow;

            //Console.WriteLine($"Found {total} files, analyzing...");

            //foreach (var path in filePaths)
            //{
            //    var fileData = fileAnalyzer.Analyze(path);
            //    var verifyFileData = new VerifyFileData { Path = path };

            //    if (verifyParameters.LogProgress)
            //    {
            //        var now = DateTime.UtcNow;
            //        if ((now - lastTime).TotalSeconds >= 10)
            //        {
            //            Console.WriteLine("Analyzed {0} files or {1}%", current, current * 100 / total);
            //            lastTime = now;
            //        }

            //        current++;
            //    }
            //}
        }

        private async Task<List<string>> CollectFiles(string path, bool recurse)
        {
            var searchOption = recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            return await Task.Factory.StartNew<List<string>>(() => Directory.GetFiles(path, "*", searchOption).ToList());
        }

        private void AssertEqual(VerifyFileData testFileData, IFileData fileData)
        {
            string contextMsg = fileData.Path;
            Assert.Equal(testFileData.Path, fileData.Path, contextMsg);
            try
            {
                Assert.Equal(testFileData.Hash, fileData.Hash, contextMsg);
            }
            catch (FormatException ex)
            {
                Log.Error(ex, $"Exception while getting Hash value for path {testFileData.Path}");
            }
            Assert.Equal(testFileData.Original, fileData.Original, 24 * 60 * 60 * 1000, contextMsg);
        }
    }
}
