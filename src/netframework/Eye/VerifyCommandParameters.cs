﻿using Marsonsoft.CommandLineParser;

namespace Eye
{
    public class VerifyCommandParameters
    {
        [Parameter(Name = "recurse", Aliases = new[] { "r" }, Description = "Recurse subdirectories")]
        public bool Recurse { get; set; }

        [Parameter(Name = "progress", Aliases = new[] { "p" }, Description = "Log progress")]
        public bool LogProgress { get; set; }

        [Parameter(Description = "File or directory path to verify")]
        public string Path { get; set; }
    }
}
