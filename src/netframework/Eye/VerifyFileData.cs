﻿using Marsonsoft.Eye.Abstractions;
using Marsonsoft.Eye.Core;
using System;
using System.Globalization;
using System.IO;

namespace Eye
{
    internal class VerifyFileData : IFileData
    {
        public string Path { get; set; }

        public byte[] Hash
        {
            get
            {
                int startIndex = Path.LastIndexOf('-');
                int endIndex = Path.LastIndexOf('.');
                string hash = Path.Substring(startIndex + 1, endIndex - startIndex - 1);
                try
                {
                    return Hex.StringToByteArray(hash);
                }
                catch (FormatException ex)
                {
                    throw new FormatException($"Error while decoding value {hash} to bytes for path {Path}", ex);
                }
            }
        }

        public DateTime Created
        {
            get
            {
                return (new FileInfo(Path)).CreationTimeUtc;
            }
        }

        public DateTime Modified
        {
            get
            {
                return (new FileInfo(Path)).LastWriteTimeUtc;
            }
        }

        public DateTime Original
        {
            get
            {
                string fileName = System.IO.Path.GetFileName(Path);
                return DateTime.ParseExact(fileName.Substring(0, 19), "yyyy-MM-dd_HH.mm.ss", CultureInfo.InvariantCulture);
            }
        }

        public long Size
        {
            get
            {
                return (new FileInfo(Path)).Length;
            }
        }

        public string MimeType
        {
            get
            {
                string extension = System.IO.Path.GetExtension(Path);
                if (string.Equals(extension, "mp4", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(extension, "avi", StringComparison.OrdinalIgnoreCase))
                {
                    return "video/" + extension;
                }
                else
                {
                    return "image/" + extension;
                }
            }
        }
    }
}
