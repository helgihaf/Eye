﻿using Marsonsoft.Eye.MediaInfoAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EyeBrowser
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(MainForm_DragEnter);
            this.DragDrop += new DragEventHandler(MainForm_DragDrop);
        }

        public void ShowMediaFileProperties(string filePath)
        {
            Text = filePath;
            var properties = GetProperties(filePath);
            PropertiesToListView(properties);

            pictureBox.SizeMode = PictureBoxSizeMode.Normal;
            pictureBox.Image = new Bitmap(filePath);
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private string GetPropertyLines(string filePath)
        {
            using (var mediaInfo = new MediaInfo.DotNetWrapper.MediaInfo())
            {
                string text = mediaInfo.Option("Info_Version");

                ////Information about MediaInfo
                //text += "\r\n\r\nInfo_Parameters\r\n";
                //text += mediaInfo.Option("Info_Parameters");

                //text += "\r\n\r\nInfo_Capacities\r\n";
                //text += mediaInfo.Option("Info_Capacities");

                //text += "\r\n\r\nInfo_Codecs\r\n";
                //text += mediaInfo.Option("Info_Codecs");

                ////An example of how to use the library
                //text += "\r\n\r\nOpen\r\n";
                mediaInfo.Open(filePath);

                //text += "\r\n\r\nInform with Complete=false\r\n";
                //mediaInfo.Option("Complete");
                //text += mediaInfo.Inform();

                //text += "\r\n\r\nInform with Complete=true\r\n";
                mediaInfo.Option("Complete", "1");
                text += mediaInfo.Inform();

                //text += "\r\n\r\nCustom Inform\r\n";
                //mediaInfo.Option("Inform", "General;File size is %FileSize% bytes");
                //text += mediaInfo.Inform();

                //foreach (string param in new[] { "BitRate", "BitRate/String", "BitRate_Mode" })
                //{
                //    text += "\r\n\r\nGet with Stream=Audio and Parameter='" + param + "'\r\n";
                //    text += mediaInfo.Get(StreamKind.Audio, 0, param);
                //}

                //text += "\r\n\r\nGet with Stream=General and Parameter=46\r\n";
                //text += mediaInfo.Get(StreamKind.General, 0, 46);

                //text += "\r\n\r\nCount_Get with StreamKind=Stream_Audio\r\n";
                //text += mediaInfo.CountGet(StreamKind.Audio);

                //text += "\r\n\r\nGet with Stream=General and Parameter='AudioCount'\r\n";
                //text += mediaInfo.Get(StreamKind.General, 0, "AudioCount");

                //text += "\r\n\r\nGet with Stream=Audio and Parameter='StreamCount'\r\n";
                //text += mediaInfo.Get(StreamKind.Audio, 0, "StreamCount");

                //text += "\r\n\r\nDispose / Close\r\n";

                return text;
            }
        }

        private void PropertiesToListView(IEnumerable<KeyValuePair<string, string>> properties)
        {
            listView.Items.Clear();
            listView.BeginUpdate();
            foreach (var property in properties)
            {
                var listViewItem = new ListViewItem();
                SetListViewItem(listViewItem, property);
                listView.Items.Add(listViewItem);
            }
            listView.EndUpdate();
            columnHeader2.Width = -1;
        }

        //private void PropertiesToListView(IList<KeyValuePair<string, string>> properties)
        //{
        //    listView.Items.Clear();
        //    listView.BeginUpdate();
        //    using (var reader = new StringReader(propertyLines))
        //    {
        //        string line;
        //        while ((line = reader.ReadLine()) != null)
        //        {
        //            var listViewItem = new ListViewItem();
        //            SetListViewItem(listViewItem, line);
        //            listView.Items.Add(listViewItem);
        //        }
        //    }
        //    listView.EndUpdate();
        //    columnHeader2.Width = -1;
        //}

        private void SetListViewItem(ListViewItem listViewItem, KeyValuePair<string, string> kvp)
        {
            listViewItem.Text = kvp.Key;
            listViewItem.SubItems.Add(kvp.Value);
        }

        //private void SetListViewItem(ListViewItem listViewItem, string line)
        //{
        //    int index = line.IndexOf(':');
        //    if (index > 1)
        //    {
        //        listViewItem.Text = line.Substring(0, index).Trim();
        //        if (index < line.Length - 1)
        //        {
        //            listViewItem.SubItems.Add(line.Substring(index + 1).Trim());
        //        }
        //    }
        //    else
        //    {
        //        listViewItem.Text = line;
        //    }
        //    listViewItem.Tag = line;
        //}

        private IEnumerable<KeyValuePair<string, string>> GetProperties(string filePath)
        {
            var adapter = new ImageAdapter();
            return adapter.ReadAllProperties(filePath);
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length >= 1)
            {
                ShowMediaFileProperties(files[0]);
            }
        }
    }
}