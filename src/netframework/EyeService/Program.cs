﻿using Marsonsoft.Eye.Abstractions;
using Marsonsoft.Eye.Framework;
using System.ServiceProcess;

namespace EyeService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            var service = new Service();

            if (args.Length == 0)
            {
                Log.Loggers.Add(new EventLogger());

                ServiceBase[] servicesToRun = new ServiceBase[]
                {
                    service
                };
                ServiceBase.Run(servicesToRun);
            }
            else
            {
                Log.Loggers.Add(new ConsoleLogger());
                service.ConsoleRun(args);
            }
        }
    }
}
