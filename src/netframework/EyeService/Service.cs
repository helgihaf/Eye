﻿using Marsonsoft.Eye.Abstractions;
using Marsonsoft.Eye.Core;
using Marsonsoft.Eye.Framework;
using Marsonsoft.Eye.MediaInfoAdapters;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System;

namespace EyeService
{
    public partial class Service : ServiceBase
    {
        private readonly ScanController scanController;
        private bool stopRequested;

        public Service()
        {
            InitializeComponent();

            string repositoryDirectoryPath = GetValidDirectoryPathFromConfig("RepositoryDirectoryPath");
            string scanDirectoryPath = GetValidDirectoryPathFromConfig("ScanDirectoryPath");
            int fileActionGracePeriodMs = GetIntFromConfig("FileActionGracePeriodMs", 10000);

            IMediaInfoAdapterFactory adapterFactory = new MediaInfoAdapterFactory();
            adapterFactory.Install("image", new ImageAdapter());
            adapterFactory.Install("video", new VideoAdapter());

            IFileAnalyzer fileAnalyzer = new FileAnalyzer(adapterFactory);
            IFileOperator fileOperator = new FileOperator();
            IFileRepository fileRepository = new FileRepository(
                fileAnalyzer,
                new FileEnumerator(repositoryDirectoryPath, true, 15), 
                fileOperator);

            IFileWatcher fileWatcher = new FileWatcher();

            scanController = new ScanController(
                fileRepository,
                fileWatcher,
                fileAnalyzer,
                fileOperator,
                scanDirectoryPath,
                fileActionGracePeriodMs);

            scanController.Stopped += ScanController_Stopped;
        }

        private void ScanController_Stopped(object sender, EventArgs e)
        {
            if (!stopRequested)
            {
                Environment.Exit(1);
            }
        }

        private static string GetValidDirectoryPathFromConfig(string key)
        {
            string directoryPath = ConfigurationManager.AppSettings[key];
            try
            {
                AssertDirectoryPathIsValid(directoryPath);
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException($"Missing or invalid value for appConfig key {key}.", ex);
            }

            return directoryPath;
        }

        private static int GetIntFromConfig(string key, int defaultValue)
        {
            string value = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }

            return int.Parse(value);
        }

        private static void AssertDirectoryPathIsValid(string directoryPath)
        {
            new DirectoryInfo(directoryPath);
        }

        public void ConsoleRun(string[] args)
        {
            scanController.Start();
            System.Console.ReadKey();
            stopRequested = true;
            scanController.Stop();
        }

        protected override void OnStart(string[] args)
        {
            stopRequested = false;
            scanController.Start();
        }

        protected override void OnStop()
        {
            stopRequested = true;
            scanController.Stop();
        }
    }
}
