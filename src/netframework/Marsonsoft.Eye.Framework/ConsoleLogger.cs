﻿using Marsonsoft.Eye.Abstractions;
using System;

namespace Marsonsoft.Eye.Framework
{
    public class ConsoleLogger : ILogger
    {
        public void LogInfo(string info)
        {
            WriteLine("[Info ] " + info);
        }

        public void LogWarning(string warning)
        {
            WriteLine("[Warn ] " + warning);
        }

        public void LogError(string error)
        {
            WriteLine("[Error] " + error);
        }

        public void LogError(Exception ex, string error)
        {
            WriteLine("[Error] " + error + ": " + ex.ToString());
        }

        private static void WriteLine(string line)
        {
            Console.WriteLine(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + " " + line);
        }
    }
}
