﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marsonsoft.Eye.Framework
{
    public class EventLogger : ILogger
    {
        const string Source = "EyeService";
        const string Log = "Application";
        const int EventIdInfo = 1;
        const int EventIdWarning = 2;
        const int EventIdError = 2;

        public EventLogger()
        {
            if (!EventLog.SourceExists(Source))
                EventLog.CreateEventSource(Source, Log);
        }

        public void LogInfo(string info)
        {
            EventLog.WriteEntry(Source, info, EventLogEntryType.Information, EventIdInfo);
        }

        public void LogWarning(string warning)
        {
            EventLog.WriteEntry(Source, warning, EventLogEntryType.Warning, EventIdWarning);
        }

        public void LogError(string error)
        {
            EventLog.WriteEntry(Source, error, EventLogEntryType.Error, EventIdError);
        }

        public void LogError(Exception ex, string error)
        {
            EventLog.WriteEntry(Source, error + Environment.NewLine + ex.ToString(), EventLogEntryType.Error, EventIdError);
        }
    }
}
