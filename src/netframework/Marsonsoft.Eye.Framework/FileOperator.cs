﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Marsonsoft.Eye.Framework
{
    public class FileOperator : IFileOperator
    {
        public void Copy(string sourceFileName, string destFileName)
        {
            EnsureDirectoryOfFileExists(destFileName);
            LockedFileRetryLoop(() => File.Copy(sourceFileName, destFileName));
        }

        public void Delete(string path)
        {
            LockedFileRetryLoop(() => File.Delete(path));
        }

        public void Move(string sourceFileName, string destFileName)
        {
            EnsureDirectoryOfFileExists(destFileName);
            LockedFileRetryLoop(() => File.Move(sourceFileName, destFileName));
        }

        public void DeleteEmptyDirectories(string parentDirectoryPath)
        {
            if (String.IsNullOrEmpty(parentDirectoryPath))
            {
                throw new ArgumentException("Directory is a null reference or an empty string", nameof(parentDirectoryPath));
            }

            try
            {
                foreach (var dir in Directory.GetDirectories(parentDirectoryPath))
                {
                    DeleteEmptyDirectories(dir);
                    var entries = Directory.GetFileSystemEntries(dir);
                    if (entries.Length == 0)
                    {
                        try
                        {
                            Directory.Delete(dir);
                        }
                        catch (UnauthorizedAccessException)
                        {
                        }
                        catch (IOException)
                        {
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
            }
            catch (IOException)
            {
            }
        }

        public void MakeWritable(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            if (fileInfo.IsReadOnly)
            {
                LockedFileRetryLoop(() => fileInfo.IsReadOnly = false);
            }
        }

        private static void EnsureDirectoryOfFileExists(string filePath)
        {
            string destinationDirectoryPath = Path.GetDirectoryName(filePath);

            if (!Directory.Exists(destinationDirectoryPath))
            {
                Directory.CreateDirectory(destinationDirectoryPath);
            }
        }

        private static bool IsFileLocked(IOException exception)
        {
            const int ERROR_SHARING_VIOLATION = 32;
            const int ERROR_LOCK_VIOLATION = 33;

            int errorCode = exception.HResult & ((1 << 16) - 1);
            return errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION;
        }

        private static void LockedFileRetryLoop(Action fileOperation)
        {
            const int maxTries = 6;
            int sleepBetweenTriesMs = 400;

            bool success = false;

            for (int i = 0; !success && i < maxTries; i++)
            {
                try
                {
                    fileOperation();
                    success = true;
                }
                catch (IOException ex) when (IsFileLocked(ex))
                {
                }

                bool isLastIteration = i == maxTries - 1;

                if (!success && !isLastIteration)
                {
                    Thread.Sleep(sleepBetweenTriesMs);
                    sleepBetweenTriesMs += sleepBetweenTriesMs;
                }
            }
        }
    }
}
