﻿using System;
using Marsonsoft.Eye.Abstractions;
using System.IO;

namespace Marsonsoft.Eye.Framework
{
    public sealed class FileWatcher : IFileWatcher, IDisposable
    {
        private FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();

        public FileWatcher()
        {
            fileSystemWatcher = new FileSystemWatcher();

            fileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName;

            fileSystemWatcher.Changed += new FileSystemEventHandler(OnChanged);
            fileSystemWatcher.Created += new FileSystemEventHandler(OnChanged);
            fileSystemWatcher.Deleted += new FileSystemEventHandler(OnChanged);
            fileSystemWatcher.Renamed += new RenamedEventHandler(OnRenamed);
        }

        public string Path
        {
            get
            {
                return fileSystemWatcher.Path;
            }
            set
            {
                fileSystemWatcher.Path = value;
            }
        }

        public bool IsStarted
        {
            get
            {
                return fileSystemWatcher.EnableRaisingEvents;
            }
        }

        public event EventHandler<EventArgs> FilesAvailable;

        public void Dispose()
        {
            if (fileSystemWatcher != null)
            {
                fileSystemWatcher.Dispose();
                fileSystemWatcher = null;
            }
        }

        public void Start()
        {
            fileSystemWatcher.EnableRaisingEvents = true;
        }

        public void Stop()
        {
            fileSystemWatcher.EnableRaisingEvents = false;
        }

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            OnFilesAvailable();
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            OnFilesAvailable();
        }

        private void OnFilesAvailable()
        {
            FilesAvailable?.Invoke(this, EventArgs.Empty);
        }
    }
}