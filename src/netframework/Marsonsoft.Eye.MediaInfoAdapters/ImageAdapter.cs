﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text;

namespace Marsonsoft.Eye.MediaInfoAdapters
{
    public class ImageAdapter : IMediaInfoAdapter
    {
        public Abstractions.MediaInfo Read(string filePath)
        {
            var result = new Abstractions.MediaInfo
            {
                FilePath = filePath
            };

            var fileInfo = new FileInfo(filePath);
            result.CreationTime = fileInfo.CreationTimeUtc;
            result.ModifiedTime = fileInfo.LastWriteTimeUtc;
            result.FileSize = fileInfo.Length;
            using (FileStream fs = fileInfo.OpenRead())
            {
                using (Image image = Image.FromStream(fs, false, false))
                {
                    // First try the ExifDTOrig: The date and time when the original image data was generated.
                    string takenTime = GetImagePropertyValue(image, PropertyTagId.ExifDTOrig) as string;
                    if (takenTime != null)
                    {
                        result.OriginalTime = ParseCameraDateTimeProperty(takenTime);
                    }
                    else
                    {
                        // Then try the DateTime property: The date and time of image creation. 
                        takenTime = GetImagePropertyValue(image, PropertyTagId.DateTime) as string;
                        if (takenTime != null)
                        {
                            result.OriginalTime = ParseCameraDateTimeProperty(takenTime);
                        }
                    }

                    result.CameraMake = GetImagePropertyValue(image, PropertyTagId.EquipMake) as string;
                }
            }

            return result;
        }

        public static object GetImagePropertyValue(Image image, PropertyTagId tagId)
        {
            object result = null;

            PropertyItem propertyItem = null;
            try
            {
                propertyItem = image.GetPropertyItem((int)tagId);
            }
            catch (ArgumentException)
            {
            }

            if (propertyItem != null)
            {
                return GetImagePropertyValue(propertyItem);
            }

            return result;
        }

        public static object GetImagePropertyValue(PropertyItem property)
        {
            object propValue = null;
            try
            {
                switch ((PropertyTagType)property.Type)
                {
                    case PropertyTagType.ASCII:
                        ASCIIEncoding encoding = new ASCIIEncoding();
                        propValue = encoding.GetString(property.Value, 0, property.Len - 1);
                        break;
                    case PropertyTagType.Int16:
                        propValue = BitConverter.ToInt16(property.Value, 0);
                        break;
                    case PropertyTagType.SLONG:
                    case PropertyTagType.Int32:
                        propValue = BitConverter.ToInt32(property.Value, 0);
                        break;
                    case PropertyTagType.SRational:
                    case PropertyTagType.Rational:
                        UInt32 numberator = BitConverter.ToUInt32(property.Value, 0);
                        UInt32 denominator = BitConverter.ToUInt32(property.Value, 4);

                        if (denominator != 0)
                            propValue = ((double)numberator / (double)denominator).ToString();
                        else
                            propValue = "0";

                        if (propValue.ToString() == "NaN")
                            propValue = "0";
                        break;
                    case PropertyTagType.Undefined:
                        propValue = "Undefined Data";
                        break;
                }

            }
            catch (ArgumentException)
            {
            }
            return propValue;
        }

        public IList<KeyValuePair<string, string>> ReadAllProperties(string filePath)
        {
            var list = new List<KeyValuePair<string, string>>();
            var fileInfo = new FileInfo(filePath);
            using (FileStream fs = fileInfo.OpenRead())
            {
                using (Image image = Image.FromStream(fs, false, false))
                {
                    foreach (var propertyItem in image.PropertyItems)
                    {
                        list.Add(new KeyValuePair<string, string>(Enum.GetName(typeof(PropertyTagId), propertyItem.Id), GetImagePropertyValue(propertyItem)?.ToString()));
                    }
                }
            }

            return list;
        }

        private static DateTime? ParseCameraDateTimeProperty(string date)
        {
            DateTime dateTimeResult;
            if (DateTime.TryParseExact(date, "yyyy:MM:dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTimeResult))
                return dateTimeResult;
            else
                return null;
        }

    }
}
