﻿using Marsonsoft.Eye.Abstractions;
using System;
using MI = global::MediaInfo.DotNetWrapper;

namespace Marsonsoft.Eye.MediaInfoAdapters
{
    public class VideoAdapter : IMediaInfoAdapter
    {
        private static string[] originalDateProperties = new[]
        {
            "Recorded_Date",
            "Released_Date",
            "Original/Released_Date",
            "Encoded_Date",
            "Tagged_Date",
            "Mastered_Date",
        };
        private static string[] creationDateProperties = new[]
        {
            "File_Created_Date",
            "File_Created_Date_Local",
        };

        private static string[] modifiedDateProperties = new[]
        {
            "File_Modified_Date",
            "Written_Date",
            "File_Modified_Date_Local",
        };

        public Abstractions.MediaInfo Read(string filePath)
        {
            var result = new Abstractions.MediaInfo
            {
                FilePath = filePath
            };

            var fileInfo = new System.IO.FileInfo(filePath);
            result.CreationTime = fileInfo.CreationTimeUtc;
            result.ModifiedTime = fileInfo.LastWriteTimeUtc;
            result.FileSize = fileInfo.Length;

            using (MI.MediaInfo mi = new MI.MediaInfo())
            {
                mi.Open(filePath);
                mi.Option("Inform", "General");

                result.OriginalTime = FindFirstDate(mi, originalDateProperties);
                result.CreationTime = FindFirstDateOrDefault(mi, creationDateProperties, result.CreationTime);
                result.ModifiedTime = FindFirstDateOrDefault(mi, modifiedDateProperties, result.ModifiedTime);
            }

            return result;
        }

        private DateTime? FindFirstDate(MI.MediaInfo mi, string[] properties)
        {
            foreach (var property in properties)
            {
                var stringValue = mi.Get(MI.StreamKind.General, 0, property);
                if (!string.IsNullOrWhiteSpace(stringValue))
                {
                    DateTime value;
                    if (TryParseDateTime(stringValue, out value))
                    {
                        return value;
                    }
                }
            }

            return null;
        }

        private DateTime FindFirstDateOrDefault(MI.MediaInfo mi, string[] properties, DateTime defaultValue)
        {
            var dateTime = FindFirstDate(mi, properties);
            if (dateTime.HasValue)
            {
                return dateTime.Value;
            }
            else
            {
                return defaultValue;
            }
        }

        private bool TryParseDateTime(string s, out DateTime value)
        {
            if (DateTime.TryParse(s, out value))
            {
                return true;
            }

            // UTC 2009-12-05 02:03:55.330
            if (DateTime.TryParseExact(s, "UTC yyyy-MM-dd HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out value))
            {
                return true;
            }

            // 2017-06-21 21:06:30.820
            if (DateTime.TryParseExact(s, "yyyy-MM-dd HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out value))
            {
                return true;
            }

            // THU APR 21 17:29:19 2011
            if (DateTime.TryParseExact(s, "ddd MMM dd HH:mm:ss yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out value))
            {
                return true;
            }

            return false;
        }
    }
}
