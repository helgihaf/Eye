﻿using Marsonsoft.Eye.Abstractions;

namespace Marsonsoft.Eye.Abstractions
{
    public interface IFileAnalyzer
    {
        IFileData Analyze(string filePath);
    }
}