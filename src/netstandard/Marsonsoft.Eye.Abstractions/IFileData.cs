﻿using System;

namespace Marsonsoft.Eye.Abstractions
{
    /// <summary>
    /// Information about a file managed by Beholder.
    /// </summary>
    public interface IFileData
    {
        /// <summary>
        /// Gets the relative path of the file.
        /// </summary>
        string Path { get; }

        /// <summary>
        /// Gets the MD5 hash of the file.
        /// </summary>
        byte[] Hash { get; }

        /// <summary>
        /// Gets the DateTime when the file was created.
        /// </summary>
        DateTime Created { get; }

        /// <summary>
        /// Gets the DateTime when the file was last modified.
        /// </summary>
        DateTime Modified { get; }

        /// <summary>
        /// Gets the DateTime when the contents of the file (image/video) occurred.
        /// </summary>
        DateTime Original { get; }

        /// <summary>
        /// Gets the size of the file in bytes.
        /// </summary>
        long Size { get; }

        /// <summary>
        /// Gets the MIME type of the file.
        /// </summary>
        string MimeType { get; }
    }
}
