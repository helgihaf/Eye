﻿using System.Collections.Generic;

namespace Marsonsoft.Eye.Abstractions
{
    public interface IFileEnumerator
    {
        string DirectoryPath { get; }
        IEnumerable<string> EnumerateFiles();
    }
}