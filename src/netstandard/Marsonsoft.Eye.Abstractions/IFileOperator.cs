﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Eye.Abstractions
{
    public interface IFileOperator
    {
        void Copy(string sourceFileName, string destFileName);
        void Delete(string path);
        void Move(string sourceFileName, string destFileName);
        void DeleteEmptyDirectories(string parentDirectoryPath);
        void MakeWritable(string filePath);
    }
}
