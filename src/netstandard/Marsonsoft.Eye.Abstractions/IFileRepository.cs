﻿namespace Marsonsoft.Eye.Abstractions
{
    public interface IFileRepository
    {
        string DirectoryPath { get; }

        string AddFile(string filePath);

        void Build();

        bool RemoveFile(string filePath);
    }
}