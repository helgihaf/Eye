﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Eye.Abstractions
{
    public interface IFileWatcher
    {
        string Path { get; set; }

        void Start();

        void Stop();

        bool IsStarted { get; }

        event EventHandler<EventArgs> FilesAvailable;
    }
}
