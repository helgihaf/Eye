﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Eye.Abstractions
{
    public interface ILogger
    {
        void LogInfo(string info);
        void LogWarning(string warning);
        void LogError(string error);
        void LogError(Exception ex, string error);
    }
}
