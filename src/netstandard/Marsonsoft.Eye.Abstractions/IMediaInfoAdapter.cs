﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Eye.Abstractions
{
    public interface IMediaInfoAdapter
    {
        MediaInfo Read(string filePath);
    }
}
