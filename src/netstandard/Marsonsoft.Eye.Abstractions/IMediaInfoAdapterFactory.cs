﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Eye.Abstractions
{
    public interface IMediaInfoAdapterFactory
    {
        IMediaInfoAdapter GetAdapter(string mimeType);

        void Install(string mimeType, IMediaInfoAdapter adapter);
    }
}
