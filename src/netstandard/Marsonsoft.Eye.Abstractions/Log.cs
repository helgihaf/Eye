﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Eye.Abstractions
{
    public static class Log
    {
        public static IList<ILogger> Loggers { get; } = new List<ILogger>();

        public static void Error(string error)
        {
            ((List<ILogger>)Loggers).ForEach(logger => logger.LogError(error));
        }

        public static void Error(Exception ex, string error)
        {
            ((List<ILogger>)Loggers).ForEach(logger => logger.LogError(ex, error));
        }

        public static void Info(string info)
        {
            ((List<ILogger>)Loggers).ForEach(logger => logger.LogInfo(info));
        }

        public static void Warning(string warning)
        {
            ((List<ILogger>)Loggers).ForEach(logger => logger.LogWarning(warning));
        }
    }
}
