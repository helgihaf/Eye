﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Eye.Abstractions
{
    public class MediaInfo
    {
        public string FilePath { get; set; }
        public DateTime? OriginalTime { get; set; }
        public long FileSize { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime ModifiedTime { get; set; }
        public string CameraMake { get; set; }
    }

}
