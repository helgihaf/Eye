﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Eye.Abstractions
{
    public class RepositoryCorruptException : RepositoryException
    {
        public RepositoryCorruptException() { }
        public RepositoryCorruptException(string message) : base(message) { }
        public RepositoryCorruptException(string message, Exception inner) : base(message, inner) { }
    }
}
