﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.IO;

namespace Marsonsoft.Eye.Core
{
    internal static class Ensure
    {
        public static void Directory(string directoryPath)
        {
            Log.Info("Ensuring writable directory " + directoryPath);
            try
            {
                var info = System.IO.Directory.CreateDirectory(directoryPath);
                var tempFilePath = Path.Combine(directoryPath, Guid.NewGuid().ToString("N"));
                File.WriteAllText(tempFilePath, "EnsureDirectoryTest");
                File.Delete(tempFilePath);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when ensuring directory " + directoryPath);
                throw;
            }
        }
    }
}
