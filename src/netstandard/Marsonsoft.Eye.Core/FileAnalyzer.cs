﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.IO;
using System.Security.Cryptography;

namespace Marsonsoft.Eye.Core
{
    public class FileAnalyzer : IDisposable, IFileAnalyzer
    {
        private readonly IMediaInfoAdapterFactory adapterFactory;
        private HashAlgorithm hashAlgorithm = MD5.Create();
        private bool disposedValue = false;

        public FileAnalyzer(IMediaInfoAdapterFactory adapterFactory)
        {
            this.adapterFactory = adapterFactory ?? throw new ArgumentNullException(nameof(adapterFactory));
        }

        public IFileData Analyze(string filePath)
        {
            if (filePath == null)
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            string extension = Path.GetExtension(filePath);
            string mimeType = Marsonsoft.Eye.Mime.Mapper.Instance.GetMimeType(extension);
            if (!IsImageOrVideo(mimeType))
            {
                throw new ArgumentException($"Extension (mime type) {extension} ({mimeType}) not supported.");
            }

            var mediaInfoAdapter = adapterFactory.GetAdapter(mimeType);
            if (mediaInfoAdapter == null)
            {
                throw new InvalidOperationException($"No adapter found for extension (mime type) {extension} ({mimeType})");
            }
            var mediaInfo = mediaInfoAdapter.Read(filePath);

            return new FileData
            {
                Path = filePath,
                Created = mediaInfo.CreationTime,
                Modified = mediaInfo.ModifiedTime,
                Size = mediaInfo.FileSize,
                MimeType = mimeType,
                Hash = CalculateHash(filePath),
                Original = CalculateOriginal(mediaInfo)
            };
        }

        private DateTime CalculateOriginal(MediaInfo mediaInfo)
        {
            if (mediaInfo.OriginalTime.HasValue) return mediaInfo.OriginalTime.Value;
            if (mediaInfo.CreationTime < mediaInfo.ModifiedTime) return mediaInfo.CreationTime;
            return mediaInfo.ModifiedTime;
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (hashAlgorithm != null)
                    {
                        hashAlgorithm.Dispose();
                        hashAlgorithm = null;
                    }
                }

                disposedValue = true;
            }
        }

        private byte[] CalculateHash(string path)
        {
            using (var stream = File.OpenRead(path))
            {
                return hashAlgorithm.ComputeHash(stream);
            }
        }

        private bool IsImageOrVideo(string mimeType)
        {
            return mimeType != null && (mimeType.StartsWith("image/") || mimeType.StartsWith("video/"));
        }
    }
}
