﻿using Marsonsoft.Eye.Abstractions;
using System;

namespace Marsonsoft.Eye.Core
{
    public class FileData : IFileData
    {
        public FileData()
        {
        }

        public FileData(IFileData fileData)
        {
            Path = fileData.Path;
            Hash = fileData.Hash;
            Created = fileData.Created;
            Modified = fileData.Modified;
            Original = fileData.Original;
            Size = fileData.Size;
            MimeType = fileData.MimeType;
        }

        public string Path { get; set; }
        public byte[] Hash { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Original { get; set; }
        public long Size { get; set; }
        public string MimeType { get; set; }
    }
}
