﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Marsonsoft.Eye.Core
{
    public class FileEnumerator : IFileEnumerator
    {
        private string[] filePaths;

        public FileEnumerator(string directoryPath, bool recurse, int progressIntervalSeconds)
        {
            DirectoryPath = directoryPath ?? throw new ArgumentNullException(nameof(directoryPath));
            Recurse = recurse;
            ProgressIntervalSeconds = progressIntervalSeconds;
        }

        public string DirectoryPath { get; private set; }
        public bool Recurse { get; private set; }
        public int ProgressIntervalSeconds { get; set; }

        public IEnumerable<string> EnumerateFiles()
        {
            Log.Info($"{DirectoryPath}: Collecting files...");

            filePaths = Directory.GetFiles(DirectoryPath, "*", GetSearchOptions());

            int total = filePaths.Length;
            int current = 0;
            var lastTime = DateTime.UtcNow;
            int lastPercentage = 0;

            Log.Info($"{DirectoryPath}: Found {total} files.");

            foreach (var path in filePaths)
            {
                yield return path;

                var now = DateTime.UtcNow;
                if ((now - lastTime).TotalSeconds >= ProgressIntervalSeconds)
                {
                    lastPercentage = current * 100 / total;
                    LogProgress(current, lastPercentage);
                    lastTime = now;
                }

                current++;
            }

            if (lastPercentage != 100)
            {
                LogProgress(total, 100);
            }
        }

        private SearchOption GetSearchOptions()
        {
            return
                Recurse ?
                    SearchOption.AllDirectories
                    : SearchOption.TopDirectoryOnly;
        }

        private void LogProgress(int current, int percentage)
        {
            Log.Info(string.Format($"{DirectoryPath}: Processed {current} files ({percentage}%)", current, percentage));
        }
    }
}
