﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marsonsoft.Eye.Core
{
    public class FileRepository : IFileRepository
    {
        private readonly IFileAnalyzer fileAnalyzer;
        private readonly IFileEnumerator fileEnumerator;
        private readonly IFileOperator fileOperator;

        private readonly IDictionary<string, string> dictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        public FileRepository(IFileAnalyzer fileAnalyzer, IFileEnumerator fileEnumerator, IFileOperator fileOperator)
        {
            this.fileAnalyzer = fileAnalyzer ?? throw new ArgumentNullException(nameof(fileAnalyzer));
            this.fileEnumerator = fileEnumerator ?? throw new ArgumentNullException(nameof(fileEnumerator));
            this.fileOperator = fileOperator ?? throw new ArgumentNullException(nameof(fileOperator));
        }

        public string DirectoryPath => fileEnumerator.DirectoryPath;

        public void Build()
        {
            dictionary.Clear();
            foreach (var filePath in fileEnumerator.EnumerateFiles())
            {
                dictionary.Add(MakeFileDataKey(filePath), filePath);
            }
        }

        public string AddFile(string filePath)
        {
            var fileData = fileAnalyzer.Analyze(filePath);
            var fileDataPath = MakeFileDataPath(fileData);
            var fileDataKey = MakeFileDataKey(fileDataPath);

            if (dictionary.TryGetValue(fileDataKey, out string existingFilePath))
            {
                if (AreFilesEqual(filePath, existingFilePath))
                {
                    throw new ArgumentException($"The media in file {filePath} already exists in the repository.");
                }
                else
                {
                    throw CreateInfiniteImprobabilityException(filePath, existingFilePath);
                }
            }
            var newFileData = new FileData(fileData);

            fileOperator.Copy(filePath, fileDataPath);
            dictionary.Add(fileDataKey, fileDataPath);

            return fileDataPath;
        }

        public bool RemoveFile(string filePath)
        {
            var fileData = fileAnalyzer.Analyze(filePath);
            var fileDataPath = MakeFileDataPath(fileData);
            var fileDataKey = MakeFileDataKey(fileDataPath);

            bool fileExists = dictionary.TryGetValue(fileDataKey, out string existingFilePath);
            if (fileExists)
            {
                if (AreFilesEqual(filePath, existingFilePath))
                {
                    fileOperator.Delete(fileDataPath);
                    dictionary.Remove(fileDataKey);
                }
                else
                {
                    throw CreateInfiniteImprobabilityException(filePath, existingFilePath);
                }
            }

            return fileExists;
        }

        private Exception CreateInfiniteImprobabilityException(string incomingFilePath, string existingFilePath)
        {
            return new RepositoryCorruptException($"A most improbable situation has occurred. File A has the same date/time properties AND the same hash value as file B but they have different contents! A={incomingFilePath}, B={existingFilePath}. Have fun redesigning your code.");
        }

        private static bool AreFilesEqual(string first, string second)
        {
            return AreFilesEqual(new FileInfo(first), new FileInfo(second));
        }

        private static bool AreFilesEqual(FileInfo first, FileInfo second)
        {
            const int BytesToRead = sizeof(Int64);

            if (first.Length != second.Length)
            {
                return false;
            }

            if (first.FullName == second.FullName)
            {
                return true;
            }

            int iterations = (int)Math.Ceiling((double)first.Length / BytesToRead);

            using (FileStream fs1 = first.OpenRead())
            using (FileStream fs2 = second.OpenRead())
            {
                byte[] one = new byte[BytesToRead];
                byte[] two = new byte[BytesToRead];

                for (int i = 0; i < iterations; i++)
                {
                    fs1.Read(one, 0, BytesToRead);
                    fs2.Read(two, 0, BytesToRead);

                    if (BitConverter.ToInt64(one, 0) != BitConverter.ToInt64(two, 0))
                        return false;
                }
            }

            return true;
        }
        private string MakeFileDataKey(string filePath)
        {
            return Path.GetFileName(filePath);
        }

        private string MakeFileDataPath(IFileData fileData)
        {
            DateTime dateTime = GetOldestDateTime(fileData);
            string fileName = string.Format("{0:0000}-{1:00}-{2:00}_{3:00}.{4:00}.{5:00}-{6}{7}",
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                dateTime.Hour,
                dateTime.Minute,
                dateTime.Second,
                Hex.ByteArrayToString(fileData.Hash),
                Path.GetExtension(fileData.Path)
                );
            return System.IO.Path.Combine(fileEnumerator.DirectoryPath, dateTime.Year.ToString(), dateTime.Month.ToString("00"), fileName);
        }

        private DateTime GetOldestDateTime(IFileData fileData)
        {
            DateTime result = fileData.Original;

            if (result > fileData.Created)
            {
                result = fileData.Created;
            }

            if (result > fileData.Modified)
            {
                result = fileData.Modified;
            }

            return result;
        }


    }
}
