﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Marsonsoft.Eye.Core
{
    public class MediaInfoAdapterFactory : IMediaInfoAdapterFactory
    {
        private readonly Dictionary<string, IMediaInfoAdapter> adapters = new Dictionary<string, IMediaInfoAdapter>();

        public void Install(string mimeType, IMediaInfoAdapter adapter)
        {
            adapters.Add(mimeType, adapter);
        }

        public IMediaInfoAdapter GetAdapter(string mimeType)
        {
            if (adapters.TryGetValue(mimeType, out IMediaInfoAdapter result))
            {
                return result;
            }

            string[] parts = mimeType.Split('/');
            if (parts.Length <= 1)
            {
                return null;
            }

            adapters.TryGetValue(parts[0], out result);

            return result;
        }
    }

}
