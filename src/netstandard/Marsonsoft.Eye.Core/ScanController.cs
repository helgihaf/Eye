﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Marsonsoft.Eye.Core
{
    public class ScanController : IDisposable
    {
        private const string InboxName = "Inbox";
        private const string OutboxName = "Outbox";
        private const string DuplicateBoxName = "Duplicate";
        private const string RejectBoxName = "Reject";

        private readonly IFileRepository fileRepository;
        private readonly IFileWatcher fileWatcher;
        private readonly IFileAnalyzer fileAnalyzer;
        private readonly IFileOperator fileOperator;
        private readonly string scanBaseDirectory;
        private readonly int fileActionGracePeriodMs;

        private readonly string inboxDirectory;
        private readonly string outboxDirectory;

        private readonly object scanMonitor = new object();

        private bool disposedValue;
        private Task workerTask;
        private CancellationTokenSource cancellationTokenSource;
        private volatile bool filesAvailable;
        private readonly Timer triggerTimer;

        public ScanController(IFileRepository fileRepository, IFileWatcher fileWatcher, IFileAnalyzer fileAnalyzer,
            IFileOperator fileOperator, string scanBaseDirectory, int fileActionGracePeriodMs)
        {
            this.fileRepository = fileRepository ?? throw new ArgumentNullException(nameof(fileRepository));
            this.fileWatcher = fileWatcher ?? throw new ArgumentNullException(nameof(fileWatcher));
            this.fileAnalyzer = fileAnalyzer ?? throw new ArgumentNullException(nameof(fileAnalyzer));
            this.fileOperator = fileOperator ?? throw new ArgumentNullException(nameof(fileOperator));
            this.scanBaseDirectory = scanBaseDirectory ?? throw new ArgumentNullException(nameof(scanBaseDirectory));
            this.fileActionGracePeriodMs = fileActionGracePeriodMs > 0 ? fileActionGracePeriodMs : 1;
            inboxDirectory = Path.Combine(scanBaseDirectory, InboxName);
            outboxDirectory = Path.Combine(scanBaseDirectory, OutboxName);

            this.fileWatcher.FilesAvailable += FileWatcher_FilesAvailable;
            triggerTimer = new Timer(TimerCallback, null, Timeout.Infinite, Timeout.Infinite);
        }

        public event EventHandler<EventArgs> Stopped;

        public void Start()
        {
            if (workerTask != null)
            {
                throw new InvalidOperationException("Already started.");
            }

            Ensure.Directory(fileRepository.DirectoryPath);
            Ensure.Directory(scanBaseDirectory);
            Ensure.Directory(inboxDirectory);
            Ensure.Directory(outboxDirectory);

            Log.Info("Initializing watcher...");
            InitializeInboxWatcher();

            cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;
            workerTask = Task.Factory.StartNew(() => ScanWorker(token), token, TaskCreationOptions.LongRunning, TaskScheduler.Default);

            Log.Info($"Initialized, waiting for media files arriving in {inboxDirectory}");
        }

        public void Stop()
        {
            if (workerTask == null || workerTask.IsCompleted || workerTask.IsCanceled || workerTask.IsFaulted)
            {
                return;
            }

            Log.Info("Stopping ScanWorker...");
            cancellationTokenSource.Cancel();
            PulseScanWorker();
            workerTask.Wait();
            workerTask = null;

            OnStopped();
            Log.Info("Stopped.");
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Stop();
                    if (cancellationTokenSource != null)
                    {
                        cancellationTokenSource.Dispose();
                        cancellationTokenSource = null;
                    }
                }

                disposedValue = true;
            }
        }

        private void InitializeInboxWatcher()
        {
            if (fileWatcher.IsStarted)
            {
                fileWatcher.Stop();
            }

            fileWatcher.Path = inboxDirectory;
            fileWatcher.Start();
        }

        private void FileWatcher_FilesAvailable(object sender, EventArgs e)
        {
            // Set timer to trigger after this file change + grace period. This is to avoid trying to access files while being copied.
            triggerTimer.Change(fileActionGracePeriodMs, Timeout.Infinite);
        }

        private void TimerCallback(object state)
        {
            lock (scanMonitor)
            {
                filesAvailable = true;
                Monitor.Pulse(scanMonitor);
            }
        }

        private void PulseScanWorker()
        {
            lock (scanMonitor)
            {
                Monitor.Pulse(scanMonitor);
            }
        }

        private void ScanWorker(CancellationToken token)
        {
            try
            {
                while (!token.IsCancellationRequested)
                {
                    Log.Info("Building repository...");
                    fileRepository.Build();

                    if (!token.IsCancellationRequested)
                    {
                        ScanUntilEmpty(token);
                    }

                    if (!token.IsCancellationRequested)
                    {
                        DeleteEmptyDirectoriesInInbox();
                    }

                    lock (scanMonitor)
                    {
                        while (!token.IsCancellationRequested && !filesAvailable)
                        {
                            Monitor.Wait(scanMonitor);
                        }
                        filesAvailable = false;
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    Log.Error(ex, "Unexpected exception in ScanWorker");
                    workerTask = null;
                    OnStopped();
                }
                catch
                {
                }

                throw;
            }
        }

        private void DeleteEmptyDirectoriesInInbox()
        {
            fileOperator.DeleteEmptyDirectories(inboxDirectory);
        }

        private void ScanUntilEmpty(CancellationToken token)
        {
            string sessionId = MakeSessionId();
            bool foundFile = true;
            while (foundFile && !token.IsCancellationRequested)
            {
                foundFile = Scan(sessionId, token);
            }
        }

        private bool Scan(string sessionId, CancellationToken token)
        {
            var fileEnumerator = new FileEnumerator(inboxDirectory, true, 30);
            bool foundFile = false;

            foreach (var filePath in fileEnumerator.EnumerateFiles())
            {
                if (token.IsCancellationRequested)
                {
                    break;
                }
                foundFile = true;

                HandleMediaFile(sessionId, filePath);
            }

            return foundFile;
        }

        private void HandleMediaFile(string sessionId, string filePath)
        {
            IFileData fileData = AnalyzeFile(filePath);

            if (fileData != null)
            {
                var addResult = AddFile(filePath);

                if (addResult is AddResultSuccess)
                {
                    var success = (AddResultSuccess)addResult;
                    fileOperator.Delete(filePath);
                    Log.Info(success.Message);
                }
                else if (addResult is AddResultDuplicate)
                {
                    MoveToOutbox(sessionId, DuplicateBoxName, filePath);
                    Log.Warning(addResult.Message);
                }
                else
                {
                    Log.Error(addResult.Message);
                }
            }
            else
            {
                MoveToOutbox(sessionId, RejectBoxName, filePath);
            }
        }

        private IFileData AnalyzeFile(string filePath)
        {
            IFileData fileData = null;
            try
            {
                fileOperator.MakeWritable(filePath);
                fileData = fileAnalyzer.Analyze(filePath);
            }
            catch (System.IO.IOException ex)
            {
                Log.Warning($"Cannot analyze file {filePath}: " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Log.Warning($"Cannot analyze file {filePath}: " + ex.Message);
            }

            return fileData;
        }

        private abstract class AddResult
        {
            public string Message { get; set; }
        }

        private class AddResultSuccess : AddResult
        {
            public string Path { get; set; }
        }

        private class AddResultDuplicate : AddResult
        {
        }

        private class AddResultSkip : AddResult
        {
        }

        private AddResult AddFile(string filePath)
        {
            try
            {
                string repositoryFilePath = fileRepository.AddFile(filePath);
                return new AddResultSuccess { Path = repositoryFilePath, Message = $"Added file {filePath} as {repositoryFilePath}" };
            }
            catch (ArgumentException ex)
            {
                return new AddResultDuplicate { Message = ex.Message };
            }
            catch (Exception ex) when (ex is UnauthorizedAccessException || ex is IOException)
            {
                return new AddResultSkip { Message = ex.Message };
            }
        }

        private string MakeSessionId()
        {
            return Guid.NewGuid().ToString("N");
        }

        private void MoveToOutbox(string sessionId, string boxName, string filePath)
        {
            string relativeFilePath = filePath.Substring(inboxDirectory.Length);
            if (relativeFilePath.StartsWith("\\"))
            {
                relativeFilePath = relativeFilePath.Substring(1);
            }
            string destinationFilePath = Path.Combine(outboxDirectory, sessionId, boxName, relativeFilePath);

            fileOperator.Move(filePath, destinationFilePath);
        }

        private void OnStopped()
        {
            Stopped?.Invoke(this, EventArgs.Empty);
        }
    }
}
