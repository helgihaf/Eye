﻿namespace Marsonsoft.Eye.Mime
{
    /// <summary>
    /// Represents a one-to-one mapping of file extensions to a MIME type and vice versa.
    /// </summary>
    public interface IMapper
    {
        /// <summary>
        /// Returns the most common file extension used for the specified MIME type.
        /// </summary>
        /// <param name="mimeType">The MIME type.</param>
        /// <returns>The most common file extension used for <paramref name="mimeType"/>, or <c>null</c> if
        /// the MIME type is not recognized.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="mimeType"/></exception>
        string GetExtension(string mimeType);

        /// <summary>
        /// Returns the most common MIME type used for the specified file extensions.
        /// </summary>
        /// <param name="fileExtension">The file extensions including the leading dot.</param>
        /// <returns>The most common MIME type used for <paramref name="fileExtension"/>, or <c>null</c> if
        /// the file extensions is not recognized.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="fileExtension"/></exception>
        string GetMimeType(string fileExtension);
    }
}