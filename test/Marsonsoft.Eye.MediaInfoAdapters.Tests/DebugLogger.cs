﻿using Marsonsoft.Eye.Abstractions;
using System;
using System.Diagnostics;

namespace Marsonsoft.Eye.MediaInfoAdapters.Tests
{
    internal class DebugLogger : ILogger
    {
        public void LogInfo(string info)
        {
            WriteLine("[Info ] " + info);
        }

        public void LogWarning(string warning)
        {
            WriteLine("[Warn ] " + warning);
        }

        public void LogError(string error)
        {
            WriteLine("[Error] " + error);
        }

        public void LogError(Exception ex, string error)
        {
            WriteLine("[Error] " + error + ": " + ex.ToString());
        }

        private static void WriteLine(string line)
        {
            Debug.WriteLine(line);
        }
    }
}