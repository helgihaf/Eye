using Marsonsoft.Eye.Abstractions;
using Marsonsoft.Eye.Core;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Xunit;

namespace Marsonsoft.Eye.MediaInfoAdapters.Tests
{
    public class SimpleTests
    {
        private readonly IServiceProvider serviceProvider;

        public SimpleTests()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            serviceProvider = serviceCollection.BuildServiceProvider();
        }

        private void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ILogger>(p => new DebugLogger());
            serviceCollection.AddSingleton<IMediaInfoAdapterFactory>(p => new MediaInfoAdapterFactory());
            serviceCollection.AddTransient<FileAnalyzer>();
        }


        //[Fact]
        //public void CollectFiles_FilesExist_FilesCollectedOk()
        //{
        //    var images = new ImageRepository("Images");
        //    var fileCollector = new FileCollector();
        //    var files = fileCollector.Enumerate(images.BasePath).ToList();
        //    foreach (var image in images.Images)
        //    {
        //        var fileData = files.FirstOrDefault(fd => fd.Path == image.Path);

        //    }
        //}

        [Fact]
        public void MyTestMethod()
        {
            var adapterFactory = serviceProvider.GetService<IMediaInfoAdapterFactory>();
            adapterFactory.Install("image", new ImageAdapter());
            adapterFactory.Install("video", new VideoAdapter());
            var fileAnalyzer = serviceProvider.GetService<FileAnalyzer>();

            foreach (var path in Directory.EnumerateFiles("Images"))
            {
                IFileData fileData = fileAnalyzer.Analyze(path);
                Assert.NotNull(fileData);
                Assert.Equal(path, fileData.Path);
                TestFileData testFileData = new TestFileData { Path = path };
                AssertEqual(testFileData, fileData);
            }
        }

        private void AssertEqual(TestFileData testFileData, IFileData fileData)
        {
            Assert.Equal(testFileData.Path, fileData.Path);
            Assert.Equal(testFileData.Hash, fileData.Hash);
            AssertEqual(testFileData.Created, fileData.Created);
            AssertEqual(testFileData.Modified, fileData.Modified);
            AssertEqual(testFileData.Original, fileData.Original);
            Assert.Equal(testFileData.Size, fileData.Size);
        }

        private void AssertEqual(DateTime expected, DateTime actual)
        {
            var ts = expected - actual;
            Assert.True(Math.Abs(ts.TotalMilliseconds) < 1000);
        }

        private string CreateTempDirectory()
        {
            var guid = Guid.NewGuid();
            var dir = Path.Combine(Path.GetTempPath(), "Beholder", guid.ToString("N"));
            Directory.CreateDirectory(dir);
            return dir;
        }
    }
}
