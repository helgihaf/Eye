using System;
using Xunit;

namespace Marsonsoft.Eye.Mime.Tests
{
    public class MapperTests
    {
        [Theory]
        [InlineData(".jpg", "image/jpeg")]
        [InlineData(".jpeg", "image/jpeg")]
        [InlineData(".xml", "application/xml")]
        [InlineData(".json", "application/json")]
        [InlineData(".mp4", "video/mp4")]
        public void GetMimeType_ValidFileExtension_MimeTypeCorrect(string extension, string expectedMime)
        {
            var mimeType = Marsonsoft.Eye.Mime.Mapper.Instance.GetMimeType(extension);
            Assert.Equal(expectedMime, mimeType);
        }

        [Theory]
        [InlineData("image/jpeg", ".jpeg")]
        [InlineData("image/x-citrix-jpeg", ".jpeg")]
        [InlineData("application/xml", ".xml")]
        [InlineData("application/json", ".json")]
        public void GetExtension_ValidMimeType_ExtensionCorrect(string mimeType, string expectedExtension)
        {
            var extension = Marsonsoft.Eye.Mime.Mapper.Instance.GetExtension(mimeType);
            Assert.Equal(expectedExtension, extension);
        }

        [Fact]
        public void GetMimeType_NullExtension_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>("fileExtension",
                () => Marsonsoft.Eye.Mime.Mapper.Instance.GetMimeType(null));
        }

        [Theory]
        [InlineData("")]
        [InlineData("850D2D8E402745DFAFB356E019396C32")]
        public void GetMimeType_UnknownExtension_ReturnsNull(string extension)
        {
            var mimeType = Marsonsoft.Eye.Mime.Mapper.Instance.GetMimeType(extension);
            Assert.Null(mimeType);
        }
    }
}
